# C&A | Teste técnico
Teste técnico para vaga de Front-end Developer na empresa C&A. 
>Para executar a aplicação é necessário ter o NodeJS e NPM instalados.

Preparar o ambiente da aplicação:
```sh
$ npm run prepare
```

### Versão estática
Executar a versão estática:
```sh
$ npm run static
```
Acesse através do: [http://localhost:9000](http://localhost:9000)

### Versão dinâmica
Executar a versão dinâmica
```sh
$ npm run dynamic
```
Acesse atraves do: [http://localhost:3000](http://localhost:3000)


Data de entrega: 04/04/2015