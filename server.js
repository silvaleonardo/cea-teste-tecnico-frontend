var express = require('express'),
    bodyParser = require('body-parser'),
    db = require('./server/db').db,
    port = process.env.PORT || 3000,
    app = express();

app.set('views', __dirname + '/client');
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/client/assets'));
app.use(express.static(__dirname + '/client/partials'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function(req, res) {
  res.render('index-dynamic', db);
});

app.listen(port, function() {
  console.log('Acesse o servidor pela url: http://localhost:%s', port);
});