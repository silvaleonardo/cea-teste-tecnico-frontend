module.exports = function(grunt) {

  // load modules
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-less');

  // configure GruntJS
  grunt.initConfig({
    // directories
    dirs: {
      src: 'client'
    },

    // watch files modification
    watch: {
      options: {
        livereload: true,
        atBegin: true,
        spawn: false
      },
      less: {
        files: '<%= dirs.src %>/{,**/}*.less',
        tasks: ['less:dev']
      }
    },

    // server loads
    connect: {
      server: {
        options: {
          port: 9000,
          base: '<%= dirs.src %>',
          hostname: 'localhost',
          livereload: true,
          open: true,
          index: 'index-static.html'
        }
      }
    },

    // compile less files
    less: {
      dev: {
        options: {
          paths: ["<%= dirs.src %>/assets/less"]
        },
        files: {
          "<%= dirs.src %>/assets/stylesheets/style.css": "<%= dirs.src %>/assets/less/style.less"
        }
      }
    }
  });

  // register tasks
  // compile less files to css
  grunt.registerTask('lesstocss',['less:dev']);

  // server loads and monitoring
  grunt.registerTask('serve', 'Iniciando servidor e monitoramento', function (target) {
    grunt.task.run(['connect', 'watch']);
  });

  // Build simple
  grunt.registerTask('build',['less:dev']);
};