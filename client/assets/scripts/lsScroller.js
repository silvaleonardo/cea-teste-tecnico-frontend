/*
* jQuery plugin created for scroller 
* elements of the technical test C&A.
*
* @author Leonardo Silva
*/

;(function($) {

  var defaults = {
    elVisible: 1,
    itemWidth: 1920, /* px */
    autoplay: true
  };
  
  // plugin constructor
  function LsScroller(el, opt) {
    var self = this;
    this.el = el;
    this.$el = $(this.el);
    this.opts = $.extend({}, defaults, opt);
    this.helpers = {};

    this.init();
  }

  // create methods for plugin
  LsScroller.prototype = {
    // function constructor
    init: function() {
      this.helpers = {
        buttons: this.$el.find('[data-navigation]'),
        overflow: this.$el.find('.overflow'),
        elScroll: this.$el.find('.overflow ul'),
        scrollItems: this.$el.find('.overflow ul li'),
        itemActive: 0,
        itemInterval: null,
        buttonClick: false
      };

      this.config();

      if(this.helpers.scrollItems.length <= this.opts.elVisible) {
        this.helpers.buttons.hide();
        return;
      }

      this.cloneElements();
      this.actionButton();
      this.scrollPlay();
    },
    // set configs
    config: function() {
      var self = this,
          ovWidth = self.helpers.overflow.width(),
          itemWidth = self.helpers.scrollItems.width();

      if(itemWidth > ovWidth) {
        self.helpers.scrollItems.innerWidth(ovWidth);
      } else {
        self.opts.elVisible = Math.floor(ovWidth / self.opts.itemWidth);
        self.helpers.scrollItems.innerWidth((ovWidth / self.opts.elVisible));
      }

      self.helpers.elScroll.width((self.helpers.scrollItems.innerWidth() * self.helpers.scrollItems.length) + 'px');
    },
    // clone elements for infinite scroller
    cloneElements: function() {
      var self = this;

      for(var i = 0, l = self.opts.elVisible; i < l; i++) {
        $(self.helpers.scrollItems[i]).clone().appendTo(self.helpers.elScroll);
      }
      
      this.helpers.scrollItems = this.$el.find('.overflow ul li');
      this.helpers.elScroll.width((self.helpers.scrollItems.innerWidth() * (this.helpers.scrollItems.length)) + 'px');
    },
    // set buttons action
    actionButton: function() {
      var self = this;
      self.helpers.buttons.on('click', function(event) {
        event.preventDefault();
        if(self.helpers.buttonClick) return;
        self.helpers.buttonClick = true;
        self.scrollStop();
        if($(this).data('navigation') === 'next') self.activeItem(true);
        else self.activeItem(false);
      });
    },
    // control scroll actions - play
    scrollPlay: function() {
      var self = this;

      if(!self.opts.autoplay) return;

      this.helpers.itemInterval = window.setTimeout(function(event) {
        self.scrollStop();
        self.activeItem(true);
      }, 4000);
    },
    // control scroll actions - stop
    scrollStop: function() {
      if(!this.opts.autoplay) return;

      window.clearTimeout(this.helpers.itemInterval);
      this.helpers.itemInterval = null;
    },
    // active item
    activeItem: function(next) {
      if(next) {
        if(this.helpers.itemActive >= (this.helpers.scrollItems.length - this.opts.elVisible)) {
          this.helpers.itemActive = 0;
          this.helpers.elScroll.css({marginLeft: this.helpers.itemActive});
        }

        this.helpers.itemActive++;
      } else {
        if(this.helpers.itemActive === 0) {
          this.helpers.itemActive = this.helpers.scrollItems.length - this.opts.elVisible;
          this.helpers.elScroll.css({marginLeft: -(this.helpers.itemActive * this.helpers.scrollItems.innerWidth()) + 'px'});
        }

        this.helpers.itemActive--;     
      }

      this.animate();
    },
    // animate the scroll
    animate: function() {
      var self = this;
      self.helpers.elScroll.stop(true, true).animate({marginLeft: -(self.helpers.itemActive * self.helpers.scrollItems.innerWidth()) + 'px'}, 500, function(event) {
        if(self.helpers.itemInterval === null) self.scrollPlay();

        self.helpers.buttonClick = false;
      });
    },
    // reset on resize window
    resizeWindow: function() {
      var rTime = new Date(1, 1, 2000, 12, 00, 00),
          timeOut = false,
          delta = 200,
          self = this;

      $(window).resize(function (event) {
        rTime = new Date();
        if(timeOut === false) {
          timeOut = true;
          setTimeout(resizeend, delta);
        }
      });

      function resizeend () {
        if(new Date() - rTime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeOut = false;
          self.config();
        }
      }
    }
  };

  // create a plugin
  $.fn.lsScroller = function(options) {
    return this.each(function() {
      new LsScroller(this, options);
    });
  };

})(jQuery);