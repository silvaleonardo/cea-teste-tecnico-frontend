// lightbox.js

(function($) {
  // toggle checkbox
  $('.more-details label[class*="color-"').on('click tap', function(event) {
    event.preventDefault();

    $('.more-details label[class*="color-"').each(function(ind, el) {
      $(el).removeClass('checked');
      $(el).children('input').removeAttr('checked');
    });

    $(this).addClass('checked');

    $(this).children('input').attr('checked', 'checked'); 
  });
})(jQuery);