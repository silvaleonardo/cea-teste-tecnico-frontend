// main.js

(function($) {
  // load plugin lsScroller
  $('.ms-banner').lsScroller();
  $('.shop-most-viewed-scroller').lsScroller({
    itemWidth: 180,
    autoplay: false
  });

  // add menu mobile action
  $('.toggle-nav-mobile').on('click tap', function(event) {
    $('body').toggleClass('open-menu-mobile');
  });

  // open lightbox
  $('.product-list .product-item').on('click tap', function(event) {
    event.preventDefault();

    var path = $(this).data('pathname') || 'partials/lightbox.html';

    // start fancybox
    $.fancybox.open({
      padding : 0,
      href: path,
      type: 'iframe',
      width: 850,
      height: 495
    });
  });  
})(jQuery);