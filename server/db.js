var db = {};

db.menu = [
  {
    title: 'Homem',
    link: '#link-homem'
  },
  {
    title: 'Mulher',
    link: '#link-mulher'
  },
  {
    title: 'Cabelo',
    link: '#link-cabelo'
  },
  {
    title: 'Dêo-Colonia',
    link: '#link-deo-colonia'
  },
  {
    title: 'Rosto',
    link: '#link-rosto'
  },
  {
    title: 'Pele',
    link: '#link-pele'
  },
  {
    title: 'Novidades',
    link: '#link-novidades'
  }
];

db.banners = [
  
];

db.banners = { 
  principal: [
    {
      title: 'FashionTronics',
      link: '#link-do-banner',
      image: 'images/banners/fashiontronics.jpg'
    },
    {
      title: 'Andrea Marques',
      link: '#link-do-banner',
      image: 'images/banners/andrea-marques.jpg'
    },
    {
      title: 'Banner Home NK',
      link: '#link-do-banner',
      image: 'images/banners/banner-home-nk.jpg'
    },
    {
      title: 'Special Denim',
      link: '#link-do-banner',
      image: 'images/banners/special-denim.jpg'
    }
  ],
  small: [
    {
      title: 'Banner 1',
      link: '#link-do-banner',
      image: 'images/banners-small/banner-1.jpg'
    },
    {
      title: 'Banner 2',
      link: '#link-do-banner',
      image: 'images/banners-small/banner-2.jpg'
    },
    {
      title: 'Banner 3',
      link: '#link-do-banner',
      image: 'images/banners-small/banner-3.jpg'
    }
  ],
  aside: [
    {
      title: 'Banner 1',
      link: '#link-do-banner',
      image: 'images/banners-aside/banner-1.jpg'
    },
    {
      title: 'Banner 2',
      link: '#link-do-banner',
      image: 'images/banners-aside/banner-2.jpg'
    }
  ]
};

db.categories = [
  {
    name: 'Lorem Ipsum',
    subs: [
      {
        title: 'Duis cursus efficitur',
        link: '#link-subcategory'
      },
      {
        title: 'Curabitur et vehicula',
        link: '#link-subcategory'
      },
      {
        title: 'Nulla felis dolor',
        link: '#link-subcategory'
      }
    ]
  },
  {
    name: 'Lorem Ipsum',
    subs: [
      {
        title: 'Duis cursus efficitur',
        link: '#link-subcategory'
      },
      {
        title: 'Curabitur et vehicula',
        link: '#link-subcategory'
      },
      {
        title: 'Nulla felis dolor',
        link: '#link-subcategory'
      }
    ]
  },
  {
    name: 'Lorem Ipsum',
    subs: [
      {
        title: 'Duis cursus efficitur',
        link: '#link-subcategory'
      },
      {
        title: 'Curabitur et vehicula',
        link: '#link-subcategory'
      },
      {
        title: 'Nulla felis dolor',
        link: '#link-subcategory'
      }
    ]
  },
  {
    name: 'Lorem Ipsum',
    subs: [
      {
        title: 'Duis cursus efficitur',
        link: '#link-subcategory'
      },
      {
        title: 'Curabitur et vehicula',
        link: '#link-subcategory'
      },
      {
        title: 'Nulla felis dolor',
        link: '#link-subcategory'
      }
    ]
  }
];

db.products = [
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: 'lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur',
    image: 'images/product/product-1.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Hidratante Andiroba Linha Amazônica',
    description: 'lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur',
    image: 'images/product/product-2.png',
    frete: false,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Delineador Líquido',
    description: 'lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur',
    image: 'images/product/product-3.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: '8,50'
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: 'lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur',
    image: 'images/product/product-1.png',
    frete: true,
    prices: {
      old: '',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Hidratante Andiroba Linha Amazônica',
    description: '',
    image: 'images/product/product-2.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: '8,50'
    }
  },
  {
    link: '#link-do-produto',
    title: 'Delineador Líquido',
    description: '',
    image: 'images/product/product-3.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: false,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Hidratante Andiroba Linha Amazônica',
    description: '',
    image: 'images/product/product-2.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Delineador Líquido',
    description: '',
    image: 'images/product/product-3.png',
    frete: true,
    prices: {
      old: '',
      price: '80,00',
      parcel: '8,50'
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: ''
    }
  }
];

db.mostviewed = [
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Hidratante Andiroba Linha Amazônica',
    description: '',
    image: 'images/product/product-2.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: '8,50'
    }
  },
  {
    link: '#link-do-produto',
    title: 'Delineador Líquido',
    description: '',
    image: 'images/product/product-3.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: false,
    prices: {
      old: '',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Desodorante Spray Masculino 90ml',
    description: '',
    image: 'images/product/product-1.png',
    frete: false,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  },
  {
    link: '#link-do-produto',
    title: 'Hidratante Andiroba Linha Amazônica',
    description: '',
    image: 'images/product/product-2.png',
    frete: true,
    prices: {
      old: '150,00',
      price: '80,00',
      parcel: ''
    }
  }
];

exports.db = db;